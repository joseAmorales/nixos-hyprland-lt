{ config, lib, inputs, pkgs, ... }:

let
  unstable = import <nixos-unstable> {};
#  home-manager = import <home-manager/nixos> { config = config; pkgs = pkgs; };
in {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    <home-manager/nixos>
#    ./nix-alien.nix
#    <nixos-hardware/lenovo/yoga/7/14ARH7/amdgpu>  #  sudo nix-channel --add https://github.com/NixOS/nixos-hardware/archive/master.tar.gz nixos-hardware
                                                  #  sudo nix-channel --update
  ];



  boot.initrd.kernelModules = [ "amdgpu" ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];


  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.supportedFilesystems = [ "ntfs" "exfat" ];


  environment.shells = with pkgs; [ bash zsh xonsh ];
  users.defaultUserShell = pkgs.bash;

  networking.hostName = "somehost"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;


  # Set your time zone.
  time.timeZone = "Europe/Kyiv";

  # Select internationalisation properties.
#  i18n.defaultLocale = "uk_UA.UTF-8";
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "uk_UA.UTF-8";
    LC_IDENTIFICATION = "uk_UA.UTF-8";
    LC_MEASUREMENT = "uk_UA.UTF-8";
    LC_MONETARY = "uk_UA.UTF-8";
    LC_NAME = "uk_UA.UTF-8";
    LC_NUMERIC = "uk_UA.UTF-8";
    LC_PAPER = "uk_UA.UTF-8";
    LC_TELEPHONE = "uk_UA.UTF-8";
    LC_TIME = "uk_UA.UTF-8";
  };



#  hardware.opengl = {
  hardware.graphics = {
    enable = true;
    enable32Bit = true;

    ## amdvlc: an open-source driver from AMD
    extraPackages = [ pkgs.amdvlk pkgs.mesa.drivers];
#    extraPackages32 = [ pkgs.driversi686Linux.amdvlk ];
  };


  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };
  programs.waybar.enable = true;
  services.greetd = {
    enable = true;
    settings = rec {
      initial_session = {
        command = "Hyprland";
        user = "me";
      };
      default_session = initial_session;
    };
  };


  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };


  # LD Fix
  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
    stdenv.cc.cc.lib
    zlib

    xorg.libX11
    xorg.libX11.dev
    xorg.libXrender
    xorg.libXxf86vm
    xorg.libXfixes
    xorg.libXi
    xorg.libSM
    xorg.libICE
    xorg.libXext
    libGL
    libxkbcommon
#    libstdcxx5
    stdenv.cc.cc.lib
    libz
  ];

#  programs.obs-studio = {
#    enable = true;
#    plugins = with pkgs.obs-studio-plugins; [
#      obs-websocket
#    ];
#  };


  # Enable CUPS to print documents.
  services.printing.enable = true;

#  security.polkit.enable = true;

  # Enable sound with pipewire.
  security.rtkit.enable = true;
  hardware.pulseaudio = {
    enable = false;
    extraConfig = "load-module module-switch-on-connect";
  };
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  services.udisks2.enable = true;
  services.devmon.enable = true;
  #services.gvfs.enable = true;

  # Bluetooth
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = true;
    settings.General.Experimental = true;
  };

  hardware.rtl-sdr.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.groups.plugdev = {};
  users.users.me = {
    isNormalUser = true;
    description = "me";
    extraGroups = [
	"networkmanager"
	"wheel"
  "audio"
  "realtime"
  "dialout"
  "plugdev"
  "storage"
  "libvirtd"
    ];
    packages = with pkgs; [
    ];
  };
  

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;


  programs.steam = {
    enable = true;
#    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
#    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
#    localNetworkGameTransfers.openFirewall = true; # Open ports in the firewall for Steam Local Network Game Transfers
  };


  environment.systemPackages = with pkgs; [
    (vim_configurable.customize {
      name = "vim";
      vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
        start = [
          vim-airline
          vim-nix
        ];
        # manually loadable by calling `:packadd $plugin-name`
        opt = [];
        # To automatically load a plugin when opening a filetype,
        # add vimrc lines like:
        # autocmd FileType php :packadd phpCompletion
      };
      vimrcConfig.customRC = ''
        :syn on         " Syntax highlighting
        :set ruler
        set nocompatible
        ":set spell      " Spell checking
        set showmatch   " When a bracket is inserted, briefly jump to a matching one
        set incsearch   " Incremental search
        set backspace=indent,eol,start
        set ignorecase
        set smartcase


        "set clipboard^=unnamed,unnamedplus
        "set clipboard+=unnamedplus
        set clipboard=unnamedplus
        xnoremap "+y y:call system("wl-copy", @")<cr>

       " nix syntax
        "au BufRead,BufNewFile *.nix set filetype=nix

        " --- Tab settings ---
        set tabstop=2
        set shiftwidth=2
        " Expand tabs for Python coding only (C/C++ in Blender uses tabs)
        set expandtab
        set smarttab

        " ---- Indenting ----
        set autoindent    " Auto indent
        set smartindent   " Smart indent
        set ci            " C/C++ indents
        set cin

        " --- Column/Row Stuff ---
        "set cul                     " Highlight the current line
        ":set number relativenumber   " Show line numbers
        ":set nu rnu
        ":set lines=40 columns=120    " Window size
        ":set colorcolumn=120

        " --- Extra functionality helpers ---
        filetype plugin on
        filetype indent on
        filetype plugin indent on

        " haskell-vim
        " https://github.com/neovimhaskell/haskell-vim
        let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
        let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
        let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
        let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
        let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
        let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
        let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

        " auto-complete
        set ofu=syntaxcomplete#Complete

        " --- Macros ---
        let @c = 'I#'
        let @u = '0xj'
      '';
    })

    home-manager

    cmake
    cpio
    git
    meson
    neovim
    nix-index
    ranger
    wl-clipboard

    kitty
    firefox
    alsa-utils
    pavucontrol
    hyprpaper

    steam-run

    #''
    libgccjit
    libatomic_ops
    
    glew SDL2
    glib
    glibc
    libnotify
    libsForQt5.qtstyleplugin-kvantum
    libsForQt5.qt5ct
#    nautilus
    brightnessctl
    xorg.xrandr
    xdg-desktop-portal-hyprland
    xdg-desktop-portal-gnome
    xdg-desktop-portal-gtk
    arc-theme
    rofi-wayland

    acpi
    ani-cli
    bat
    bluetuith
    bluez
    cmus
    curl
    exfat
    exfatprogs
    ffmpeg
    file
    gcc
    gotop
    grim
    hlint
    htop
    iftop
    imagemagick
    killall
    lazygit
    lm_sensors
    lsof
    nmap
    neofetch
    nethack
    pciutils
    rustc rustup cargo
#    testdisk
    testdisk
    tldr
    tree
    unzip
    usbutils
    virtiofsd
    w3m
    wev
    wget
    wirelesstools iwd
    which
    woeusb
    xclip

#    betaflight-configurator
    brave
    imv
    mpv
    qutebrowser
#    renderdoc   # A single-frame graphics debugger
    slurp
    spotify
    sxiv
    xorg.xkill
    wofi
#    zathura
#    zathura-with-plugins
  
    qjackctl

    #''
  ];
  nixpkgs.config.packageOverrides = pkgs: {
    unstable = unstable;
  };

  
  # Fonts
  fonts.fontDir.enable = true;
  fonts.packages = with pkgs; [
    fira-code
    mononoki
    inconsolata-nerdfont
    liberation_ttf   # For Steam
    ricty  # A high-quality Japanese font based on Inconsolata and Migu 1M
  ];

  environment.sessionVariables = {
    EDITOR = "nvim";
  };


  environment.variables = {
    XDG_CURRENT_DESKTOP = "Hyprland";
    XDG_SESSION_TYPE = "wayland";
    WRL_DRM_NO_ATOMIC = "1";
    GDK_BACKEND = "wayland, x11";
    QT_QPA_PLATFORM = "wayland, xcb";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    QT_SCALE_FACTOR = "1.2";
#    QT_STYLE_OVERRIDE = "kvantum";
    QT_STYLE_OVERRIDE = "adwaita-dark";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    QT_QPA_PLATFORM_PLUGIN_PATH = "${pkgs.libsForQt5.qt5.qtbase.bin}/lib/qt-${pkgs.libsForQt5.qt5.qtbase.version}/plugins";
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:


  xdg.portal.enable = true;
  services.upower.enable = true;
  services.flatpak.enable = true;
  services.emacs.enable = true;
  services.emacs.package = with pkgs; (emacs.pkgs.withPackages (with emacs.pkgs; [
    smex
    nix-mode
    haskell-mode
    python-mode
    lua-mode
    multiple-cursors
    magit
    hindent
    sclang-extensions
  ]));

  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;


  services.locate.enable = true;
  services.locate.package = pkgs.mlocate;
  services.locate.localuser = null;



  # Power
  services.logind.extraConfig = ''
    # don't shut down when power button is short-pressed
    HandlePowerKey=ignore
    LidSwitchIgnoreInhibited=no
  '';


  services.udev.packages = [

    (pkgs.writeTextFile {
      name = "usbserial_udev";
      text = ''
        # DFU (Internal bootloader for STM32 and AT32 MCUs)
        ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="2e3c", ATTRS{idProduct}=="df11", MODE="0664", GROUP="plugdev"
        ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="0664", GROUP="plugdev"
        # HackRF
        ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="6089", MODE="660", GROUP="plugdev"
        '';
        destination = "/etc/udev/rules.d/45-stdfu-permissions.rules";
    })
  ];


  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
