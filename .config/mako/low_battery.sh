
LOW_THRESHOLD=25
HIGH_THRESHOLD=85
FULL_THRESHOLD=100

BATTERY_STATUS=$(cat /sys/class/power_supply/BAT0/status)
BATTERY_CAPACITY=$(cat /sys/class/power_supply/BAT0/capacity)

if [ "$BATTERY_STATUS" == "Full" ] || [ "$BATTERY_CAPACITY" -eq 100 ] && [ "$BATTERY_STATUS" == "Charging" ]; then
  notify-send -t 0 -c batterygreen ""
fi

if [ "$BATTERY_CAPACITY" -ge "$HIGH_THRESHOLD" ] && [ "$BATTERY_STATUS" == "Charging" ]; then
  notify-send -t 55555 -c batterygreen ""
fi


if [ "$BATTERY_CAPACITY" -le "$LOW_THRESHOLD" ] && [ "$BATTERY_STATUS" == "Discharging" ]; then
  notify-send -t 55555 -c batteryred ""
fi

#          
