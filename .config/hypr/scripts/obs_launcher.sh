#!/usr/bin/env bash

if ! pgrep -x ".obs-wrapped" > /dev/null; then
  hyprctl dispatch exec "[workspace 10 silent] obs --disable-shutdown-check --startrecording"
fi
