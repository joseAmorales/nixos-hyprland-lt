{ config, pkgs, inputs, ... }:

let
#  nixpkgs = import <nixpkgs> {};
  unstable = (import <nixos-unstable> {});
in
{
  home.username = "me";
  home.homeDirectory = "/home/me";

  nixpkgs.config.allowUnfree = true;

  gtk = {
    enable = true;
    theme = {
      name = "Breeze-Dark";
      package = pkgs.libsForQt5.breeze-gtk;
    };
    gtk3 = {
      extraConfig.gtk-application-prefer-dark-theme = true;
    };
  };
  dconf.settings = {
    "org/gnome/desktop/interface" = {
      gtk-theme = "Breeze-Dark";
      color-scheme = "prefer-dark";
    };
  };


  qt = {
    enable = true;
    platformTheme.name = "qtct";
    style.name = "kvantum";
  };

  xdg.configFile."Kvantum/kvantum.kvconfig".source = (pkgs.formats.ini { }).generate "kvantum.kvconfig" {
    General.theme = "Catppuccin-Mocha-Pink";
  };


  xdg.mimeApps = {
    enable = true;
    associations.added = {
      "image/*" = "imv-dir.desktop";
    };
    defaultApplications = {
      "image/*" = "imv-dir.desktop";
      "application/pdf" = [ "org.pwmt.zathura.desktop" ];
    };
  };

  home.packages = with pkgs; [

    (catppuccin-kvantum.override {
      accent = "green";   # Blue Flamingo Green Lavender Maroon Mauve Peach Pink Red Rosewater Sapphire Sky Teal Yellow
      variant = "mocha";  # Latte Frappe Macchiato Mocha
    })
    libsForQt5.qtstyleplugin-kvantum
    kdePackages.qtstyleplugin-kvantum
    libsForQt5.qt5ct
    kdePackages.qt6ct

    (pkgs.haskellPackages.ghcWithPackages (ghcpkgs: with ghcpkgs; [
      apecs  # Fast Entity-Component-System library for game programming
      brick  # a declarative terminal user interface library
#      fwgl   # a library for interactive 2D and 3D applications and games
      gloss  # painless 2D vector graphics, animations and simulations
#      helm   # a functionally reactive game engine
#      hoogle  # hoogle server --local --port=8080      http://localhost:8080
      random
      split
      vty    # terminal GUI library in the niche of ncurses
      Unique
    ]))

    (pkgs.python3.withPackages (ps: with ps; [
      opencv4
    ]))


    asciiquarium
    bluez
    bluez-alsa
    bluez-tools
#    dwarf-fortress
    desktop-file-utils
    iw
    jq
    linux-wifi-hotspot
    meow
    p7zip
    platformio
    ponysay
    pulseaudio
    putty
    rpi-imager
    wl-clipboard
    wl-mirror
    xterm
    yazi

    unstable.blender
    cool-retro-term
    dosbox
#    godot_4
    inkscape
    krita
    lorien
    mixxx faad2
    obs-studio
#    obs-cmd
    qbittorrent
    discord
#    solvespace
    dolphin
    signal-desktop
    whatsapp-for-linux
    zathura

    poppler_utils

    # FPV
    betaflight-configurator electron
    edgetx
    inav-configurator
    inav-blackbox-tools
    libatomic_ops
    mission-planner
    qgroundcontrol
    sane-backends

    # Music
    ardour
#    bespokesynth #surge-XT
    milkytracker
    renoise
#    (renoise.overrideAttrs (oldAttrs: rec {
#      version = "3.4.4";
#      src = pkgs.fetchurl {
#        url = "https://files.renoise.com/demo/Renoise_3_4_4_Demo_Linux_x86_64.tar.gz";
#        sha256 = "b+YXBVnxu54HfC/tWapcs/ZYzwBOJswYbEbEU3SVNss=";
#      };
#    }))
    sfxr-qt
    supercollider

    # Radio
    sdrpp
    soapysdr
    soapyhackrf
    soapyremote
    soapysdrplay
    sigdigger
    hackrf
    cubicsdr
    sdrangel
    gnuradio
    gnuradioPackages.osmosdr
    gqrx
    xnec2c # Graphical NEC2 Antenna Simulation

    vscode

#    (import <nixos-unstable> {}).yt-dlp
    unstable.yt-dlp
#    unstable.ffmpeg

    (retroarch.override {
      cores = with libretro; [
        nestopia         # nes
        snes9x           # snes
        genesis-plus-gx  # megadrive / genesis
        beetle-gba       # ngba
        beetle-psx       # psx
        beetle-psx-hw    # psx
        pcsx-rearmed     # psx
        ppsspp           # psp
        mame             # arcade
      ];
    })

#    openmvg   # A library for computer-vision scientists and targeted for the Multiple View Geometry community

  ];
#  nixpkgs.overlays = [
#    (self: super: {
#      betaflight-configurator = super.betaflight-configurator.overrideAttrs (oldAttrs: {
#        buildInputs = oldAttrs.buildInputs ++ [ nwjsOverride ];
#      });
#    })
#  ];


  programs.bash = {
    enable = true;
    historyControl = [ "ignoredups" "ignorespace" ];
    historyFile = "$HOME/.bash_history";
    initExtra = ''
      PS1='\[\e[01;36m\]\u@\h\[\e[00m\] \[\e[01;34m\]\w\[\e[00m\] $\n'
      if [[ -n "$IN_NIX_SHELL" ]]; then
        PROMPT_COMMAND='export PS1="\[\e[01;32m\]nix-shell\[\e[01;34m\] \[\e[01;34m\]\w\[\e[00m\] \[\e[01;32m\]$\n\e[00m\]"
        unset PROMPT_COMMAND'
      fi
      ponysay -of $(find "/mnt/storage/ponies" -type f -print0 | shuf -z -n 1 | tr '\0' '\n')
    '';
    shellAliases = {
      ".." = "cd ..";
      gitlog = "git log --all --decorate --oneline --graph";
      la  = "ls -a";
      ll  = "ls -l";
      hmc = "nvim ~/.config/home-manager/home.nix";
      hms = "home-manager switch";
      ncu = "sudo nix-channel --update";
      nxc = "sudo nvim /etc/nixos/configuration.nix";
      nxr = "sudo nixos-rebuild switch";
      nml = "nmcli dev wifi list";
      nmc = "nmcli dev wifi connect EA:79:C6:78:36:4F";
      nv  = "nvim";
      p   = ''ponysay -of $(find "/mnt/storage/ponies" -type f -print0 | shuf -z -n 1 | tr '\0' '\n')'';
      ts  = ''date -u +"%Y-%m-%d_%H-%M-%s"'';
      yd  = "yt-dlp";
    };
  };

  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };



  services.mako = {
    enable = true;
#    maxVisible = 11;
    defaultTimeout = 5555;
    backgroundColor = "#000000ff"; borderSize = 11; borderRadius = 0;
    textColor = "#ffffffff";
#    textColor = "#0dcdddff";
    width = 444;
    height = 444;
    borderColor = "#0dcdddff";
#    progressColor = "#ff0000ff";
    margin = "22";
    padding = "5";
    anchor = "top-center";
    font = "FiraCode 22";
    icons = true;
    maxIconSize = 64;
    iconPath = "";

    markup = true;
    actions = true;
#    format = "<b>%s</b>\n%b";
    extraConfig = ''
      layer=overlay
      text-alignment=center
      #max-visible=1
      #max-history=0
      
      [app-name="Spotify"]
      invisible=1

      [urgency=critical]
      border-color=#FF0000FF

      [category=workspace]
      font=FiraCode 77
      background-color=#00000000
      text-color=#0dcdddff
      border-size=0

      [category=batteryred]
      font=FiraCode 99
      background-color=#00000000
      text-color=#ff0000ff
      border-size=0

      [category=batterygreen]
      font=FiraCode 99
      background-color=#00000000
      text-color=#00ff00ff
      border-size=0

      [category=pulseaudio]
      #max-visible=1
      font=FiraCode 77
      background-color=#00000000
      text-color=#0dcdddff
      border-size=0
      width=6666
      
    '';
  };

  systemd.user.services.lowBatteryNotification = {
    Unit = {
      Description = "Battery Notification Service";
    };
    Service = {
     ExecStart = "/home/me/.nix-profile/bin/bash -c '/home/me/.config/mako/low_battery.sh'";
      Environment = [
        "WAYLAND_DISPLAY=wayland-0"
        "PATH=/home/me/.nix-profile/bin:/run/current-system/sw/bin"
      ];
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
  };
  systemd.user.timers.lowBatteryNotification = {
    Unit = {
      Description = "Battery Notification Timer";
    };
    Timer = {
      OnBootSec = "2m";
      OnUnitActiveSec = "2m";
      Unit = "lowBatteryNotification.service";
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };

#  programs.obs-studio = {
#    enable = true;
#    plugins = with pkgs.obs-studio-plugins; [
#      obs-websocket
#    ];
#  };


  home.file."~/.config/ranger/rc.conf".source = /home/me/.config/ranger/rc.conf;
  home.file."~/.config/ranger/rifle.conf".source = /home/me/.config/ranger/rifle.conf;

  home.file."~/.config/mpv/mpv.conf".source = /home/me/.config/mpv/mpv.conf;
  home.file."~/.config/mpv/input.conf".source = /home/me/.config/mpv/input.conf;

#  home.file."~/.config/qutebrowser/config.py".source = /home/me/.config/qutebrowser/config.py;
  home.file."~/.config/qutebrowser/autoconfig.yml".source = /home/me/.config/qutebrowser/autoconfig.yml;


  home.file.".config/imv/config".text = ''
    [options]

    scaling_mode = shrink
    #upscaling_method = linear
    upscaling_method = nearest_neighbour
    initial_pan = 50 50
    loop_input = true
    
    # Suppress built-in key bindings, and specify them explicitly in this
    # config file.
    suppress_default_binds = true
    
    [aliases]
    # Define aliases here. Any arguments passed to an alias are appended to the
    # command.
    # alias = command to run
    
    [binds]
    # Define some key bindings
    q = quit
    y = exec echo working!
    
    # Image navigation
    <Left> = prev
    p = prev
    <bracketleft> = prev
    <Right> = next
    n = next
    <bracketright> = next
    gg = goto 1
    <Shift+G> = goto -1
    
    # Panning
    j = pan 0 -50
    k = pan 0 50
    h = pan 50 0
    l = pan -50 0
    
    # Zooming
    <Up> = zoom 1
    <Shift+plus> = zoom 1
    i = zoom 1
    <Down> = zoom -1
    <minus> = zoom -1
    o = zoom -1
    
    # Rotate Clockwise by 90 degrees
    <Ctrl+r> = rotate by 90
    
    # Other commands
    x = close
    <Shift+X> = exec rm $imv_current_file
    f = fullscreen
    d = overlay
    #p = exec echo $imv_current_file
    c = center
    s = scaling next
    <Shift+S> = upscaling next
    a = zoom actual
    r = reset
    
    # Gif playback
    <period> = next_frame
    <space> = toggle_playing
    
    # Slideshow control
    t = slideshow +1
    <Shift+T> = slideshow -1
  '';


#  home.file = {
#    ".config/imv" = {
#      source = config.lib.file.mkOutOfStoreSymlink "/home/me/.config/imv/config";
#    };
#  };


  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
